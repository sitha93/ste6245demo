#ifndef SCENES_OBJECTEXAMPLES_EXAMPLES_TEMPLATE_H
#define SCENES_OBJECTEXAMPLES_EXAMPLES_TEMPLATE_H

namespace
{
  static_assert(false, "Your not meant to compile this!!"
                       " ; Put YOUR stuff in the detail namespace"
                       " ; Construct the example(s)"
                       " ; Declare the example(s)"
                       " ; CHANGE THE PATH-RELATIVE HEADER GUARDS"
                       " ; ... and ... remove me !!!");
}

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initSomeUniqueExample(gmlib2::qt::Scenegraph* scenegraph);


  // "Local" detail resources
  // namespace detail {
  //   auto lambda_construct_my_very_special_thing = [](){};
  // } // namespace detail


  // Examples
  void initSomeUniqueExample(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    // "a parametric point"
    auto* a_ppoint = new APPoint(scenegraph);
    a_ppoint->initDefaultComponents();
    a_ppoint->defaultGeometryRenderer()->setRings(10);
    a_ppoint->defaultGeometryRenderer()->setSlices(10);
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_EXAMPLES_TEMPLATE_H
