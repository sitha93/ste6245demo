#ifndef SCENES_OBJECTEXAMPLES_PHERMITECURVE_EXAMPLES_H
#define SCENES_OBJECTEXAMPLES_PHERMITECURVE_EXAMPLES_H

// gmlib2
#include <gmlib2/qt/scenegraph.h>
#include <gmlib2/qt/exampleobjects/parametric/parametricobjects.h>

namespace examples
{

  // Declare contained examples
  void initPHermiteCurveP2V2Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPHermiteCurveP3V2Example(gmlib2::qt::Scenegraph* scenegraph);
  void initPHermiteCurveP3V3Example(gmlib2::qt::Scenegraph* scenegraph);


  // Examples
  void initPHermiteCurveP2V2Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* phermite_curve = new PHermiteCurveP2V2(
      PHermiteCurveP2V2::Point{0.0, 0.0, 0.0},
      PHermiteCurveP2V2::Point{5.0, 0.0, 0.0},
      PHermiteCurveP2V2::Vector{5.0, 5.0, 0.0},
      PHermiteCurveP2V2::Vector{5.0, 0.0, 0.0}, scenegraph);
    phermite_curve->initDefaultComponents();
    phermite_curve->defaultMesh()->setSamples(100);
  }

  void initPHermiteCurveP3V2Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* phermite_curve = new PHermiteCurveP3V2(
      PHermiteCurveP3V2::Point{0.0, 0.0, 0.0},
      PHermiteCurveP3V2::Point{2.5, 10.0, 0.0},
      PHermiteCurveP3V2::Point{5.0, 0.0, 0.0},
      PHermiteCurveP3V2::Vector{5.0, 5.0, 0.0},
      PHermiteCurveP3V2::Vector{5.0, 0.0, 0.0}, scenegraph);
    phermite_curve->initDefaultComponents();
    phermite_curve->defaultMesh()->setSamples(100);
  }

  void initPHermiteCurveP3V3Example(gmlib2::qt::Scenegraph* scenegraph)
  {
    using namespace gmlib2::qt::parametric;

    auto* phermite_curve = new PHermiteCurveP3V3(
      PHermiteCurveP3V3::Point{0.0, 0.0, 0.0},
      PHermiteCurveP3V3::Point{2.5, 5.0, 0.0},
      PHermiteCurveP3V3::Point{5.0, 0.0, 0.0},
      PHermiteCurveP3V3::Vector{5.0, 5.0, 0.0},
      PHermiteCurveP3V3::Vector{5.0, -5.0, 0.0},
      PHermiteCurveP3V3::Vector{5.0, 0.0, 0.0}, scenegraph);
    phermite_curve->initDefaultComponents();
    phermite_curve->defaultMesh()->setSamples(100);
  }

}   // namespace examples

#endif   // SCENES_OBJECTEXAMPLES_PHERMITECURVE_EXAMPLES_H
