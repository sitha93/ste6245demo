#ifndef EXAMPLES_H
#define EXAMPLES_H

// examples
#include "scenes/defaultscene_example.h"
#include "scenes/objectexamples/ppoint_examples.h"
#include "scenes/objectexamples/phermitecurve_examples.h"
#include "scenes/objectexamples/pcoonspatch_examples.h"
#include "scenes/objectexamples/ppolygon_examples.h"
#include "scenes/objectexamples/psubobject_examples.h"


namespace examples
{

  void initExampleScenario()
  {
// Default scene example
//  initDefaultSceneExample(m_scenegraph);

// Parametric point
//  initParametricPointExample(m_scenegraph);

// Parametric curve examples
//  initPHermiteCurveP2V2Example(m_scenegraph);
//  initPHermiteCurveP3V2Example(m_scenegraph);
//  initPHermiteCurveP3V3Example(m_scenegraph);

// Parametric surface examples
//  initPBilinearCoonsPatchExample(m_scenegraph);
//  initPBicubicCoonsPatchExample(m_scenegraph);

// Parametric polygon surface examples
//  initPPolygon4Example(m_scenegraph);
//  initPPolygon5Example(m_scenegraph);
//  initPPolygon10Example(m_scenegraph);

// Sub-curve examples
//  initPSubCurveOnCircleExample(m_scenegraph);
//  initPSubCurveOnTorusExample(m_scenegraph);
//  initPSubCurveOnPolygon4Example(m_scenegraph);
//  initPSubSurfaceOnTorusExample(m_scenegraph);
//  initPSubSurfaceOnPolygon4Example(m_scenegraph);
//  initPSubSpaceCurveOnCircleExample(m_scenegraph);
//  initPSubSpaceCurveOnTorusExample(m_scenegraph);
//  initPSubSpaceSurfaceOnTorusExample(m_scenegraph);
//  initPSubSpacePolygonSurfaceOnPolygon10Example(m_scenegraph);
  }

}   // namespace examples
#endif   // EXAMPLES_H
