import QtQuick 2.0
import QtQuick.Scene3D 2.0

import Qt3D.Core 2.0
import Qt3D.Render 2.0
import Qt3D.Input 2.0
import Qt3D.Extras 2.0

import com.uit.GMlib2Qt 1.0

import "../../shared_assets/cameras"
import "../../shared_assets/framegraphs"
import "../../shared_assets/environments"

Item {
  anchors.leftMargin: 10
  anchors.topMargin: 10
  anchors.rightMargin: 10
  anchors.bottomMargin: 10

  default property alias content: scene_root.childNodes
  property string scene_element_name: "scene_root_entity"
  property alias scenemodel: scene_root.scenemodel

  Item {
    id: view_controls
    property var active_camera : camera_set.cameras[3]

    property var singleViewAspectRatio : 16/9
    property var quadViewportCenter: center_positioner.quadViewportCenter()

    onSingleViewAspectRatioChanged: updateSingleViewAspectRatios()
    onQuadViewportCenterChanged: updateQuadViewAspectRatios()
    onStateChanged: {
      updateSingleViewAspectRatios()
      updateQuadViewAspectRatios()
    }

    Component.onCompleted: {
      singleViewAspectRatio = Qt.binding(function() {return overlay.width/overlay.height})
    }

    function updateSingleViewAspectRatios() {
      if(view_controls.state === "single_view") {
        single_viewport_framegraph.camera.aspectRatio = singleViewAspectRatio
      }
    }
    function updateQuadViewAspectRatios() {
      if(view_controls.state === "quad_view" ){
        var viewportCenter = view_controls.quadViewportCenter
        var tl_ar = viewportCenter.x / viewportCenter.y * singleViewAspectRatio
        var tr_ar = (1.0-viewportCenter.x) / viewportCenter.y * singleViewAspectRatio
        var bl_ar = viewportCenter.x / (1.0-viewportCenter.y) * singleViewAspectRatio
        var br_ar = (1.0-viewportCenter.x) / (1.0-viewportCenter.y) * singleViewAspectRatio

        quad_viewport_framegraph.topLeftCamera.aspectRatio = tl_ar
        quad_viewport_framegraph.topRightCamera.aspectRatio = tr_ar
        quad_viewport_framegraph.bottomLeftCamera.aspectRatio = bl_ar
        quad_viewport_framegraph.bottomRightCamera.aspectRatio = br_ar
      }

    }

    state: "single_view"
    states: [
      State {
        name: "single_view"
        PropertyChanges { target: render_settings
          activeFrameGraph: single_viewport_framegraph }
        PropertyChanges { target: click_area
          hoverEnabled: false }
        PropertyChanges { target: center_positioner
          enabled: false }
        PropertyChanges { target: horizontal_divider
          visible: false }
        PropertyChanges { target: vertical_divider
          visible: false }
      },
      State {
        name: "quad_view"
        PropertyChanges { target: render_settings
          activeFrameGraph: quad_viewport_framegraph }
        PropertyChanges { target: click_area
          hoverEnabled: true }
        PropertyChanges { target: center_positioner
          enabled: true }
        PropertyChanges { target: horizontal_divider
          visible: true }
        PropertyChanges { target: vertical_divider
          visible: true }
      }
    ]
  }

  Item {
    id: overlay
    z: 1
    anchors.fill: scene3d

    Item {
      id: center_positioner

      x: overlay.width * 0.33
      y: overlay.height * 0.33
      z: 2
      width: 10
      height: width

      MouseArea{
        anchors.fill: parent
        drag.target: center_positioner
        drag.axis: Drag.XAndYAxis
      }

      function center() {
        return Qt.point(
              (x+width/2),
              (y+height/2)
              )
      }

      function quadViewportCenter() {
        var c = center()
        return Qt.point(
              c.x/overlay.width,
              c.y/overlay.height
              )
      }
    }

    Item {
      id: horizontal_divider
      x: overlay.x
      y: center_positioner.y
      z: 1
      width: overlay.width
      height: center_positioner.height

      Rectangle {
        x: parent.x
        anchors.verticalCenter: parent.verticalCenter
        width: parent.width
        height: 2
        color: "gray"
        border.color: "black"
        border.width: 1
      }

      MouseArea{
        anchors.fill: parent
        drag.target: center_positioner
        drag.axis: Drag.YAxis
      }
    }

    Item {
      id: vertical_divider
      x: center_positioner.x
      y: overlay.y
      z: 1
      width: center_positioner.width
      height: overlay.height

      Rectangle {
        anchors.horizontalCenter: parent.horizontalCenter
        y: parent.y
        width: 2
        height: parent.height
        color: "gray"
        border.color: "black"
        border.width: 1
      }

      MouseArea{
        anchors.fill: parent
        drag.target: center_positioner
        drag.axis: Drag.XAxis
      }
    }

    MouseArea {
      id: click_area
      anchors.fill: parent

      onPositionChanged: {


        var coords = Qt.point(mouse.x/click_area.width,mouse.y/click_area.height)
        if(hoverEnabled)
          overlay.setActiveCamera(coords)
      }

      onDoubleClicked: {

        var coords = Qt.point(mouse.x/click_area.width,mouse.y/click_area.height)

        if(view_controls.state === "single_view") {
          view_controls.state = "quad_view"
        }
        else if(view_controls.state === "quad_view") {
          view_controls.state = "single_view"
          overlay.setActiveCamera(coords)
        }
      }
    }

    function setActiveCamera( coords ) {
      var center = center_positioner.quadViewportCenter()

      if(coords.x <= center.x && coords.y <= center.y)
        view_controls.active_camera = quad_viewport_framegraph.topLeftCamera
      else if(coords.x > center.x && coords.y <= center.y)
        view_controls.active_camera = quad_viewport_framegraph.topRightCamera
      else if(coords.x <= center.x && coords.y > center.y)
        view_controls.active_camera = quad_viewport_framegraph.bottomLeftCamera
      else
        view_controls.active_camera = quad_viewport_framegraph.bottomRightCamera
    }
  }

  Scene3D {
    id: scene3d
    objectName: "scene3d"
    anchors.fill: parent
    focus: true
    aspects: ["input","logic"]

    Entity {
      id: root_entity

      components: [ input_settings, render_settings]

      RenderSettings { id: render_settings
        activeFrameGraph: single_viewport_framegraph
        pickingSettings.pickMethod: PickingSettings.TrianglePicking
      }

      InputSettings { id: input_settings }

      SingleViewportFrameGraph {
        id: single_viewport_framegraph
        camera: view_controls.active_camera
      }

      QuadViewportFrameGraph {
        id: quad_viewport_framegraph
        topLeftCamera: camera_set.cameras[0]
        topRightCamera: camera_set.cameras[1]
        bottomLeftCamera: camera_set.cameras[2]
        bottomRightCamera: camera_set.cameras[3]
        viewportCenter: center_positioner.quadViewportCenter()
      }

      Entity {
        id: camera_set
        property var cameras: [
          iso_front_camera, iso_top_camera,
          iso_side_camera, projection_camera]

        ProjectionCamera {
          id: projection_camera
          position: Qt.vector3d(0.0, -20.0, 20.0)
          viewCenter: Qt.vector3d(0.0, 1.0, 0.0) }

        ProjectionCamera {
          id: iso_top_camera
          position: Qt.vector3d(0.0, 0.0, -50.0)
          viewCenter: Qt.vector3d(0.0, 0.0, 0.0)
          upVector: Qt.vector3d(0.0,1.0,0.0) }

        ProjectionCamera {
          id: iso_side_camera
          position: Qt.vector3d(-50.0, 0.0, 0.0)
          viewCenter: Qt.vector3d(0.0, 0.0, 0.0) }

        ProjectionCamera {
          id: iso_front_camera
          position: Qt.vector3d(0.0, -50.0, 0.0)
          viewCenter: Qt.vector3d(0.0, 0.0, 0.0) }
      }

      OrbitCameraController{ camera: view_controls.active_camera }

      SceneRootEntity {
        id: scene_root
        objectName: scene_element_name
      }

      DefaultSkyboxLightEnvironment {}

    } // END Entity (id:scene_root)

  } // END Scene3D


}
