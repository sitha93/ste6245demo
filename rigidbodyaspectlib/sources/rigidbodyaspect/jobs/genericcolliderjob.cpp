#include "genericcolliderjob.h"

#include "../constants.h"
#include "../backend/rigidbodyaspect.h"
#include "../backend/environmentbackend.h"

#include "../algorithms/collision.h"
#include "../algorithms/simulation.h"

#include <set>
#include <vector>
#include <tuple>
// qt
#include <QDebug>

namespace rigidbodyaspect
    {

  GenericColliderJob::GenericColliderJob(RigidBodyAspect* aspect)
    : m_aspect{aspect}
  {
  }

  void GenericColliderJob::setFrameTimeDt(seconds_type dt) { m_dt = dt; }

  void GenericColliderJob::run()
  {
      C = {};

      auto& rigid_bodies = m_aspect->rigidBodies();

      for(auto& sphere : rigid_bodies.spheres()) {
          if(sphere.state != MovingBody::SphereState::Rest) {
              const auto env_id = sphere.m_env_id;
              const auto env_BE = m_aspect->environmentBackend(env_id);
              const auto Gn     = env_BE ? env_BE->m_gravity : GM2Vector{0, 20.0, 0};
              const auto F      = Gn;
              sphere.initPosAndTime(m_dt, F);

              for(auto& fixed_sphere : rigid_bodies.fixedSpheres()) {
                  algorithms::collision::detail::ColliderReturnType col = algorithms::collision::detectCollisionwithSurroundingSphere(sphere, m_dt, fixed_sphere);
                  if(size_t(std::get<1>(col)) ==1){
                      auto myCollision = std::get<algorithms::collision::detail::CollisionObject>(col);
                      myCollision.sphere = &sphere;
                      C.push_back(myCollision);
                  }
              }


              //Collisions with planes
              for(auto& fixed_plane : rigid_bodies.fixedPlanes()) {
                  algorithms::collision::detail::ColliderReturnType col = algorithms::collision::detectCollisionSpherePlane(sphere, fixed_plane,m_dt);
                  if(size_t(std::get<1>(col)) ==1){
                      auto myCollision = std::get<algorithms::collision::detail::CollisionObject>(col);
                      myCollision.sphere = &sphere;
                      myCollision.plane = &fixed_plane;
                      C.push_back(myCollision);
                  }
              }

              //Collision with other spheres
              for(auto& otherSphere : rigid_bodies.spheres()) {
                  if(&sphere != &otherSphere) {
                      algorithms::collision::detail::ColliderReturnType col = algorithms::collision::detectCollisionWithOtherSphere(sphere, otherSphere, m_dt);
                      if(size_t(std::get<1>(col)) == 1) {
                          auto myCollision = std::get<algorithms::collision::detail::CollisionObject>(col);
                          myCollision.sphere = &sphere;
                          myCollision.secondSphere = &otherSphere;
                          C.push_back(myCollision);
                      }
                  }
              }
          }
      }

      std::sort(C.begin(), C.end(),
                      [](const algorithms::collision::detail::CollisionObject &a, const algorithms::collision::detail::CollisionObject &b) {
                          return a.dt > b.dt;
                      });

            std::set<rbtypes::Sphere*> unique_spheres;

            const auto unique_end =
                    std::remove_if(C.begin(), C.end(),
                    [&unique_spheres] (const algorithms::collision::detail::CollisionObject &col) {
                      auto *sphere = col.sphere;
                      auto *othersphere = col.secondSphere;
                      if(unique_spheres.count(sphere))
                          return true;
                      else
                          unique_spheres.insert(sphere);

                      if(unique_spheres.count(othersphere))
                          return true;
                      else
                          unique_spheres.insert(othersphere);
                      return false;
                    });
            C.erase(unique_end, C.end());


      while(!C.empty()) {

          auto currentCollision = C.back();
          C.pop_back();

          if(currentCollision.collidedWithSphere) {
                currentCollision.sphere->moveBodyUpToCollision(currentCollision.dt);
                currentCollision.secondSphere->moveBodyUpToCollision(currentCollision.dt);
                auto dist = blaze::length(currentCollision.sphere->frameOriginParent() - currentCollision.secondSphere->frameOriginParent());
                std::cout << dist << std::endl;

                auto velocity = currentCollision.sphere->m_velocity;
                currentCollision.sphere->calculateVelocityAfterCollision(currentCollision.dt, currentCollision.secondSphere->frameOriginParent(), currentCollision.secondSphere->m_velocity);
                currentCollision.secondSphere->calculateVelocityAfterCollision(currentCollision.dt, currentCollision.sphere->frameOriginParent(), velocity);
                auto vel1 = currentCollision.sphere->m_velocity;
                auto vel2 = currentCollision.secondSphere->m_velocity;

          }else {
              currentCollision.sphere->moveBodyThatCollidedWithPlane(currentCollision.dt, currentCollision.n);
              if(currentCollision.FixedSphereRadius != 0) {
                  currentCollision.sphere->checkStateChangeFixedSphere(currentCollision.FixedSphereRadius, currentCollision.n);
              }
          }

          //Collision with planes
          for(auto& fixed_plane : rigid_bodies.fixedPlanes()) {
              if(&fixed_plane != currentCollision.plane) {
                  algorithms::collision::detail::ColliderReturnType col = algorithms::collision::detectCollisionSpherePlane(*currentCollision.sphere, fixed_plane,currentCollision.dt);
                  if(size_t(std::get<1>(col)) ==1){
                      auto myCollision = std::get<algorithms::collision::detail::CollisionObject>(col);
                      myCollision.sphere = currentCollision.sphere;
                      myCollision.plane = &fixed_plane;
                      C.push_back(myCollision);
                  }
              }
          }

          for(auto& otherSphere : rigid_bodies.spheres()) {
              if((currentCollision.sphere != &otherSphere) && (currentCollision.secondSphere != &otherSphere)) {
                  algorithms::collision::detail::ColliderReturnType col = algorithms::collision::detectCollisionWithOtherSphere(*currentCollision.sphere, otherSphere, m_dt);
                  if(size_t(std::get<1>(col)) == 1) {
                      auto myCollision = std::get<algorithms::collision::detail::CollisionObject>(col);
                      myCollision.sphere = currentCollision.sphere;
                      myCollision.secondSphere = &otherSphere;
                      C.push_back(myCollision);
                  }
              }
          }

          std::sort(C.begin(), C.end(),
                          [](const algorithms::collision::detail::CollisionObject &a, const algorithms::collision::detail::CollisionObject &b) {
                              return a.dt > b.dt;
                          });

                std::set<rbtypes::Sphere*> unique_spheres_inner;



                const auto unique_end =
                        std::remove_if(C.begin(), C.end(),
                        [&unique_spheres] (const algorithms::collision::detail::CollisionObject &col) {
                          auto *sphere = col.sphere;
                          auto *othersphere = col.secondSphere;
                          if(unique_spheres.count(sphere))
                              return true;
                          else
                              unique_spheres.insert(sphere);

                          if(unique_spheres.count(othersphere))
                              return true;
                          else
                              unique_spheres.insert(othersphere);
                          return false;
                        });
                C.erase(unique_end, C.end());

      }

      for(auto& sphere : rigid_bodies.spheres()) {
          if(sphere.state != MovingBody::SphereState::Rest)
              sphere.moveBody(seconds_type(0));
      }

}
}   // namespace rigidbodyaspect
