#ifndef COLLISIONJOB_H
#define COLLISIONJOB_H

#include "../types.h"

#include "../algorithms/collision.h"
#include "../backend/rigidbodyaspect.h"
#include <vector>
#include <Qt3DCore/QAspectJob>

namespace rigidbodyaspect {

    class RigidBodyAspect;

class CollisionJob : public Qt3DCore::QAspectJob
{
public:
    CollisionJob(RigidBodyAspect* aspect);

    void setTimeFrameDt(seconds_type dt);

    void run() override;
private:
    RigidBodyAspect* m_aspect;
    seconds_type m_dt;
    std::vector<algorithms::collision::detail::CollisionObject> C;

};

using CollisionJobPtr = QSharedPointer<CollisionJob>;

} //end namespace rigidbody

#endif // COLLISIONJOB_H
