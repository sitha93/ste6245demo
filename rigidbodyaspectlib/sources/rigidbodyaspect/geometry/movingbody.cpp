#include "movingbody.h"

#include <iostream>

namespace rigidbodyaspect
{
void MovingBody::initPosAndTime(seconds_type dt, const GM2Vector& H)
{
    m_point_in_dt = dt;
    g = H;
    auto firstpartOfEq = m_velocity * dt.count();
    auto secondpartOfEq = 0.5 * g *dt.count() * dt.count();
    m_ds = firstpartOfEq + secondpartOfEq;
    w_dt = dt;
}

void rigidbodyaspect::MovingBody::updatePosAndTime(rigidbodyaspect::seconds_type dt)
    {
        m_point_in_dt = dt;

        auto firstpartOfEq = m_velocity * dt.count();
        auto secondpartOfEq = 0.5 * g *dt.count() * dt.count();
        m_ds = firstpartOfEq + secondpartOfEq;

    }

    void MovingBody::moveBody(seconds_type dt)
    {
        auto time_diff = m_point_in_dt -dt;
        auto time_fraction = time_diff / m_point_in_dt;
        auto path = m_ds * time_fraction;
        this->translateLocal(path);
        m_velocity += g * time_diff.count();

    }

    void MovingBody::moveBodyThatCollidedWithPlane(seconds_type dt, GM2Vector n)
    {
        auto time_diff = m_point_in_dt - dt;
        auto time_fraction = time_diff /m_point_in_dt;
        auto path = m_ds * time_fraction;

        this->translateLocal(path);

        GM2Vector velocity = m_velocity;
        velocity += g * time_diff.count();

        const auto vn = blaze::inner(velocity, n);
        velocity -= ( 2 * vn * n);
        if(state == SphereState::Free) {

        }

        m_velocity = velocity * 0.95;
        updatePosAndTime(dt);

    }

    void MovingBody::checkStateChangeFixedSphere(double radius, GM2Vector n)
    {
        if(blaze::inner(m_ds, n) < 0.00001) {
            auto check = blaze::inner(-n * 0.5, m_ds) - blaze::inner(m_ds, m_ds);
            auto anotherCheck = (blaze::length(n-GM2Vector{0,1,0}));
            if(std::abs(check) < 0.00001 && anotherCheck < 0.00001) {
                m_velocity = GM2Vector{0,0,0};
                m_ds = GM2Vector{0,0,0};
                state = SphereState::Rest;
            } else {

                auto pos = this->frameOriginParent();
                auto p = pos + m_ds;
                auto q = blaze::length(p) - radius;
                auto futureN = -p / blaze::length(p);
                auto d = futureN*(q + 0.5);
                m_ds += d;
                state = SphereState::Slide;
            }

        }
    }

    void MovingBody::moveBodyUpToCollision(seconds_type dt)
    {
        auto time_diff = m_point_in_dt - dt;
        auto time_fraction = time_diff /m_point_in_dt;
        auto path = m_ds * time_fraction;

        this->translateLocal(path);

        GM2Vector velocity = m_velocity;
        velocity += g * time_diff.count();
        m_point_in_dt = dt;

    }

    void MovingBody::calculateVelocityAfterCollision(seconds_type dt, GM2Vector posOtherSphere, GM2Vector velocityOtherSphere)
    {
        auto d = (this->frameOriginParent() -  posOtherSphere) / blaze::length(this->frameOriginParent() -  posOtherSphere);

        auto dLiv = gmlib2::algorithms::linearIndependentVector(gmlib2::DVectorT<double>(d));
        auto n = blaze::cross(dLiv, d);

        auto vZeroD = blaze::inner(velocityOtherSphere, d);
        auto vOneD = blaze::inner(m_velocity, d);
        auto vOneN = blaze::inner(m_velocity, n);

        //mass is equal.....
        auto scalarVOneD = vZeroD;

        auto newVelocity = vOneN * n + d* scalarVOneD;

        m_velocity = newVelocity;

        updatePosAndTime(dt);

    }

}
