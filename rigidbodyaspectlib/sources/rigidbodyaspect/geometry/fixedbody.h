#ifndef RIGIDBODYASPECT_FIXEDBODY_H
#define RIGIDBODYASPECT_FIXEDBODY_H

#include "../types.h"

namespace rigidbodyaspect
{

  class FixedBody : public GM2SpaceObjectType  {
  public:
    // members
      GM2Vector q = GM2Vector{0,0,0};
      double r = 5;

    Qt3DCore::QNodeId m_env_id;
  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_FIXEDBODY_H
