#ifndef RIGIDBODYASPECT_RBTYPES_H
#define RIGIDBODYASPECT_RBTYPES_H

#include "movingbody.h"
#include "fixedbody.h"

namespace rigidbodyaspect::rbtypes {

    using Sphere     = gmlib2::parametric::PSphere<MovingBody>;
//    using FixedPlane = gmlib2::parametric::PSphere<FixedBody>;
    using FixedPlane = gmlib2::parametric::PPlane<FixedBody>;
    using FixedSphere = gmlib2::parametric::PSphere<FixedBody>;

}



#endif // RIGIDBODYASPECT_RBTYPES_H
