#ifndef RIGIDBODYASPECT_MOVINGBODY_H
#define RIGIDBODYASPECT_MOVINGBODY_H

#include "../types.h"


namespace rigidbodyaspect
{

  class MovingBody : public GM2SpaceObjectType  {
  public:

    // members
    Unit_Type         m_mass;
    GM2Vector         m_velocity;
    Qt3DCore::QNodeId m_env_id;
    GM2Vector         g = GM2Vector{0,-9.8,0};


    // cache vars
    GM2Vector           m_path_travelled  = GM2Vector{0,0,0};
    GM2Vector           m_ds;
    seconds_type        m_point_in_dt;
    GM2Vector           m_p;
    seconds_type        w_dt;


    enum class SphereState {Free, Rest, Slide};
    SphereState         state = SphereState::Free;
    void initPosAndTime(seconds_type dt, const GM2Vector& H);
    void updatePosAndTime(seconds_type dt);
    void moveBody(seconds_type dt);
    void moveBodyThatCollidedWithPlane(seconds_type dt, GM2Vector n);
    void checkStateChangeFixedSphere(double radius, GM2Vector n);
    void moveBodyUpToCollision(seconds_type dt);
    void calculateVelocityAfterCollision(seconds_type dt, GM2Vector posOtherSphere, GM2Vector velocityOtherSphere);
  };

}   // namespace rigidbodyaspect

#endif // RIGIDBODYASPECT_MOVINGBODY_H

