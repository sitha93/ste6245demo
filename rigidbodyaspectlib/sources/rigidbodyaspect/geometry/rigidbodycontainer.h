#ifndef RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
#define RIGIDBODYASPECT_RIGIDBODYCONTAINER_H

#include "rbtypes.h"

namespace rigidbodyaspect
{

  class RigidBodyContainer {
  public:
    using SphereQHash     = QHash<Qt3DCore::QNodeId, rbtypes::Sphere>;
    using FixedPlaneQHash = QHash<Qt3DCore::QNodeId, rbtypes::FixedPlane>;
    using FixedSphereQHash = QHash<Qt3DCore::QNodeId, rbtypes::FixedSphere>;


    RigidBodyContainer();

    // Sphere
    void                  constructSphere(Qt3DCore::QNodeId id);
    void                  destroySphere(Qt3DCore::QNodeId id);
    rbtypes::Sphere&      sphere(Qt3DCore::QNodeId id);
    const rbtypes::Sphere sphere(Qt3DCore::QNodeId id) const;
    SphereQHash&          spheres();
    const SphereQHash&    spheres() const;

    // Plane
    void                      constructFixedPlane(Qt3DCore::QNodeId id);
    void                      destroyFixedPlane(Qt3DCore::QNodeId id);
    rbtypes::FixedPlane&      fixedPlane(Qt3DCore::QNodeId id);
    const rbtypes::FixedPlane fixedPlane(Qt3DCore::QNodeId id) const;
    FixedPlaneQHash&          fixedPlanes();
    const FixedPlaneQHash&    fixedPlanes() const;

    //FixedSpere
    void                    constructFixedSphere(Qt3DCore::QNodeId id);
    void                    destroyFixedSphere(Qt3DCore::QNodeId id);
    rbtypes::FixedSphere&   fixedSphere(Qt3DCore::QNodeId id);
    const rbtypes::FixedSphere fixedSphere(Qt3DCore::QNode id) const;
    FixedSphereQHash&       fixedSpheres();
    const FixedSphereQHash& fixedSpheres() const;

  private:
    SphereQHash     m_spheres;
    FixedPlaneQHash m_fixed_planes;
    FixedSphereQHash m_fixed_spheres;
  };

}   // namespace rigidbodyaspect

#endif   // RIGIDBODYASPECT_RIGIDBODYCONTAINER_H
