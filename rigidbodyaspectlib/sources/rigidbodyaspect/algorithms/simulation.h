#ifndef RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
#define RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H

#include "../geometry/movingbody.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body, seconds_type dt,
                                 const GM2Vector& F);

  GM2Vector simulateSphere(MovingBody& body, seconds_type dt_sec, const GM2Vector& F);

  void translateSphere(MovingBody& body);

}   // namespace rigidbodyaspect::algorithms::simulation


#endif   // RIGIDBODYASPECT_ALGORITHMS_SIMULATION_H
