#include "simulation.h"

namespace rigidbodyaspect::algorithms::simulation
{

  void simulateGenericMovingBody(MovingBody& body, seconds_type dt_sec,
                                 const GM2Vector& F)
  {
    const auto dt = dt_sec.count();

    // compute dynamics
    const auto a = F * dt;
    const auto ds = (body.m_velocity + 0.5 * a) * dt;

    // update physical properties
    body.translateLocal(ds);
    body.m_velocity += a;
  }

  GM2Vector simulateSphere(MovingBody& body, seconds_type dt_sec, const GM2Vector &F)
  {
     const auto dt = dt_sec.count();

     const auto a = F * dt;
     const auto ds = (body.m_velocity + 0.5 * a) * dt;

     body.m_velocity += a;
     return ds;
  }

  void translateSphere(MovingBody& body) {
      body.translateLocal(body.m_path_travelled);
  }





}   // namespace rigidbodyaspect::algorithms::dynamics
