#ifndef RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
#define RIGIDBODYASPECT_ALGORITHMS_COLLISION_H

#include "../types.h"
#include "../geometry/rbtypes.h"

// stl
#include <variant>


namespace rigidbodyaspect::algorithms::collision
{

  namespace detail
  {

    enum class ColliderStatus { NoCollision , Collision, CollisionSphere };
    struct CollisionObject {
        bool collidedWithSphere = false;
        double FixedSphereRadius = 0;
        blaze::StaticVector<double, 3, false> n;
        rbtypes::FixedPlane* plane;
        rbtypes::Sphere* sphere;
        rbtypes::Sphere* secondSphere;
        seconds_type dt;
    };

    using ColliderReturnType = std::pair<CollisionObject, ColliderStatus>;
  }   // namespace detail


  detail::ColliderReturnType
  detectCollisionSpherePlane(const rbtypes::Sphere&     sphere,
                         const rbtypes::FixedPlane& plane, seconds_type dt);

  detail::ColliderReturnType
  detectCollisionwithSurroundingSphere(const rbtypes::Sphere& sphere, seconds_type dt, const rbtypes::FixedSphere surroundingSphere);

  detail::ColliderReturnType
  detectCollisionWithOtherSphere(const rbtypes::Sphere& sphere, const rbtypes::Sphere& otherSphere, seconds_type dt);


}   // namespace rigidbodyaspect::algorithms::collision


#endif   // RIGIDBODYASPECT_ALGORITHMS_COLLISION_H
