#include "collision.h"
#include <iostream>
#include <cmath>

namespace rigidbodyaspect::algorithms::collision
{
//should be a return type for sphere/fixed plane, nothing more
  detail::ColliderReturnType
  detectCollisionSpherePlane(const rbtypes::Sphere&     sphere,
                         const rbtypes::FixedPlane& plane,
                         seconds_type               dt)
  {

      const auto pl_eval = plane.evaluateParent(rbtypes::FixedPlane::PSpacePoint{0.0, 0.0},
                                                rbtypes::FixedPlane::PSizeArray{1UL, 1UL});

      const auto epsilon = 0.0000001;

      const auto pl_u = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 1UL));
      const auto pl_v = blaze::subvector<0UL, 3UL>(pl_eval(1UL, 0UL));

      const auto q = blaze::subvector<0UL, 3UL>(pl_eval(0UL, 0UL));

      const auto n = blaze::normalize(blaze::cross(pl_u, pl_v));

      const auto p = sphere.frameOriginParent();

      const auto r = sphere.m_radius;

      const auto ds = sphere.m_ds;

      const auto d = (q + r * n) - p;

      const auto dn = blaze::inner(d, n);
      const auto dsn = blaze::inner(ds, n);
      const auto x = dn / dsn;

      if (x > 0 && x < 1) {
          detail::CollisionObject collision;
          collision.n = n;
          collision.dt = dt - (x*dt);
          return std::make_pair(collision, detail::ColliderStatus::Collision);
      }else {
          return std::make_pair(detail::CollisionObject(), detail::ColliderStatus::NoCollision);
      }

  }

  detail::ColliderReturnType detectCollisionwithSurroundingSphere(const rbtypes::Sphere &sphere, seconds_type dt, const rbtypes::FixedSphere surroundingSphere)
  {

      const auto p = sphere.frameOriginParent();
      const auto r = sphere.m_radius;
      const auto ds = sphere.m_ds;
      const auto R = surroundingSphere.m_radius;
      const auto Q = surroundingSphere.frameOriginParent();
      const auto distance = blaze::length(p+ds) - R + r;


      if(distance > 0) {
          detail::CollisionObject collision;
          auto dt_frac = distance / blaze::length(ds);
          collision.dt = dt * dt_frac;
          collision.FixedSphereRadius = R;
          collision.n = Q - p / blaze::length(Q - p);
          return std::make_pair(collision, detail::ColliderStatus::Collision);
      } else {
          return std::make_pair(detail::CollisionObject(), detail::ColliderStatus::NoCollision);
      }
  }

  detail::ColliderReturnType detectCollisionWithOtherSphere(const rbtypes::Sphere &sphere, const rbtypes::Sphere &otherSphere, seconds_type dt)
  {
        const auto Q =  sphere.frameOriginParent() - otherSphere.frameOriginParent();
        const auto R = sphere.m_ds - otherSphere.m_ds;
        const auto r = sphere.m_radius + otherSphere.m_radius;
        const auto QQ = blaze::inner(Q, Q);
        const auto RR = blaze::inner(R, R);
        const auto QR = blaze::inner(Q, R);

        const auto expr = blaze::sqrt((QR*QR) - (RR * (QQ - (r*r))));
        const auto x = (-QR - expr)/RR;


        if(x > 0 && x < 1) {
            detail::CollisionObject collision;
            collision.collidedWithSphere = true;
            collision.dt = dt - (x*dt);

            return std::make_pair(collision, detail::ColliderStatus::Collision);
        }

        return detail::ColliderReturnType(std::make_pair(detail::CollisionObject(), detail::ColliderStatus::NoCollision));
  }

}   // namespace rigidbodyaspect::algorithms::collision

