#include "ground.h"


namespace geometricobjects
{
bool Ground::qt_types_initialized = false;

QVector3D Ground::qtPt() const
{
    return {float(m_pt[0]), float(m_pt[1]), float(m_pt[2])};
}

QVector3D Ground::qtU() const
{
    return {float(m_u[0]), float(m_u[1]), float(m_u[2])};
}

QVector3D Ground::qtV() const
{
    return {float(m_v[0]), float(m_v[1]), float(m_v[2])};
}

void Ground::setPt(QVector3D p)
{
    m_pt = VectorH_Type{double(p[0]), double(p[1]), double(p[2]), 1.0};
    emit ptChanged(p);
}

void Ground::setU(QVector3D u)
{
    m_u = VectorH_Type{double(u[0]), double(u[1]), double(u[2]), 0.0};
    emit uChanged(u);
}

void Ground::setV(QVector3D v)
{
    m_v = VectorH_Type{double(v[0]), double(v[1]), double(v[2]), 0.0};
    emit vChanged(v);
}


}   // namespace geometricobjects


//Parametric space -> S(u,v) -> Embedded Space
