#include "ball.h"

namespace geometricobjects
{
  bool Ball::qt_types_initialized = false;

  void Ball::initMaterial(){
    material = new Qt3DExtras::QPhongAlphaMaterial(this);
    material->setAlpha(0.3);
    material->setDiffuse("cyan");
    material->setAmbient("cyan");
    this->addComponent(material);
  }

  Qt3DExtras::QPhongAlphaMaterial* Ball::ballMaterial() {
      return material;
  }

  void Ball::setFixed(bool b) {
      _fixed = b;
      initMaterial();
      emit fixedChanged();
  }
}   // namespace geometricobjects


