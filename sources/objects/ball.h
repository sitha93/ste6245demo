#ifndef BALL_H
#define BALL_H

#include <gmlib2/qt/exampleobjects/parametric/psurface.h>
#include <Qt3DExtras/QPhongAlphaMaterial>
#include <Qt3DRender/QEffect>
// qt
#include <QQmlEngine>

namespace geometricobjects
{

  class Ball : public gmlib2::qt::parametric::PSurface<
                 gmlib2::parametric::PSphere<gmlib2::qt::SceneObject>> {
    using Base = PSurface<gmlib2::parametric::PSphere<gmlib2::qt::SceneObject>>;
    Q_OBJECT

    Q_PROPERTY(
      gmlib2::qt::parametric::PSurfaceMesh* defaultMesh READ defaultMesh)
    Q_PROPERTY( Qt3DExtras::QGoochMaterial* defaultMaterial READ defaultMaterial)
    Q_PROPERTY(double radius MEMBER m_radius NOTIFY radiusChanged)
   Q_PROPERTY(QVector3D velocity MEMBER m_velocity NOTIFY velocityChanged)
      Q_PROPERTY(bool fixed WRITE setFixed NOTIFY fixedChanged)


    // Constructor(s)
  public:
    template <typename... Ts>
    Ball(Ts&&... ts) : Base(std::forward<Ts>(ts)...)
    {
      initDefaultComponents();
      connect(this, &Ball::radiusChanged, this->defaultMesh(),
              &gmlib2::qt::parametric::PSurfaceMesh::reSample);
    }

    Q_INVOKABLE void setVelocityQt(const QVector3D velocity) {
        m_velocity = velocity;
        emit velocityChanged(velocity);
    }
    Q_INVOKABLE void setMaterial() {
        initMaterial();
    }
    static void registerQmlTypes(int version_major, int version_minor)
    {
      if (qt_types_initialized) return;

      constexpr auto registertype_uri = "com.uit.STE6245";
//      qDebug() << "Trying to register type <Ball>; getting id: " <<
      qmlRegisterType<Ball>(registertype_uri, version_major, version_minor,
                            "Ball");

      qt_types_initialized = true;
    }
    void initMaterial();
    Qt3DExtras::QPhongAlphaMaterial* ballMaterial();

    void setFixed(bool b);

  signals:
    void radiusChanged();
    void velocityChanged(QVector3D);
    void fixedChanged();

  private:
    static bool qt_types_initialized;
    QVector3D m_velocity;
    Qt3DExtras::QPhongAlphaMaterial* material;
    bool _fixed = false;
  };


}   // namespace geometricobjects


#endif   // BALL_H
