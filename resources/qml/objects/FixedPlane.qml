import QtQuick 2.0

import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.STE6245 1.0 as App
import com.uit.STE6245.RigidBody 1.0 as RB

App.Ground {
    id: ground

    property alias environment: rbc.environment

    defaultMesh.samples: Qt.size(5,5)


    RB.PlaneController {
        id: rbc

        p: ground.p
        u: ground.u
        v: ground.v

        dynamicsType: RB.RigidBodyNs.StaticObject
    }
    function initPlane() {
        rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt() )
    }
}
