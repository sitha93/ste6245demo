import QtQuick 2.0
import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Extras 2.0

import com.uit.STE6245 1.0 as App
import com.uit.STE6245.RigidBody 1.0 as RB

App.Ball {
    id: fixedsphere

    property alias environment: rbc.environment

    radius: 7

    fixed: true

    defaultMesh.samples: Qt.size(40,40)

    onRadiusChanged: defaultMesh.reSample()

    RB.SphereController {
        id:  rbc

        radius: fixedsphere.radius
        dynamicsType: RB.RigidBodyNS.StaticObject
        onFrameComputed: fixedsphere.setFrameParentQt(dir, up, pos)

    }

    function initFixedSphere() {
        rbc.resetFrameByDup( directionAxisGlobalQt(), upAxisGlobalQt(), frameOriginGlobalQt())
    }
}
