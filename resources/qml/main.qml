import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB

import "objects" as Obj

ApplicationWindow {
  id: root

  function updateGravity() {
    var g = Qt.vector3d(g_x.value,g_y.value,g_z.value)
    rba_environment.gravity = g
  }

  visible: true

  width: 800
  height: 600


  menuBar: MenuBar {
    Menu {
      title: "File"
      MenuItem {
        text: "Toggle simulator";
        checkable: true; checked: scenario.simulatorRunStatus;
        onTriggered: scenario.simulatorRunStatus = !scenario.simulatorRunStatus
      }
      MenuSeparator{}
      MenuItem { text: "Quit"; onTriggered: Qt.quit() }
    }
    Menu {
      title: "Env change"
      MenuItem {
        text: "Env 1"
        onTriggered: ball_rbc.environment = rba_environment
      }
      MenuItem {
        text: "Env 2"
        onTriggered: ball_rbc.environment = rba_environment2
      }
    }
    Menu {
        title: "Choose scene"
        MenuItem {
            text: "Spheres In A Cube"
            onTriggered: scene_loader.source =  "CubeScene.qml"
        }
        MenuItem {
            text: "Inclined Planes"
            onTriggered: scene_loader.source = "StateChangeTest.qml"
        }
        MenuItem {
            text: "Inner Loop Test"
            onTriggered: scene_loader.source = "InnerLoopTest.qml"
        }
        MenuItem {
            text: "Surrounding Sphere Test"
            onTriggered: scene_loader.source = "SurroundingSphereTest.qml"
        }
    }
  }


  statusBar: Item {
    anchors.bottom: parent.bottom
    anchors.left: parent.left
    anchors.right: parent.right
    height: 40

    RowLayout {
      anchors.fill: parent
      anchors.leftMargin: 5
      anchors.rightMargin: 5

      Item { Layout.fillWidth: true }
      Text{ text: "Radius"}
      SpinBox{ decimals: 1; stepSize: 0.1; minimumValue: 0.3; maximumValue: 5; value: ball.radius;
        onValueChanged: ball.radius = value
      }
      Item{ width: 10 }
      Text{ text: "Gravity"}
      SpinBox{ id:g_x; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: 0; onValueChanged: root.updateGravity() }
      SpinBox{ id:g_y; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: -9.80; onValueChanged: root.updateGravity()  }
      SpinBox{ id:g_z; decimals: 2; stepSize: 0.05; minimumValue: -20; maximumValue: 20; value: 0; onValueChanged: root.updateGravity()  }
    }

  }

  Scenario {
    id: scenario
    anchors.fill: parent


    EntityLoader {
        id: scene_loader
    }

  }
}
