import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3

import Qt3D.Core 2.10
import Qt3D.Render 2.10
import Qt3D.Input 2.0
import Qt3D.Animation 2.10
import Qt3D.Extras 2.10

import com.uit.GMlib2Qt 1.0
import com.uit.STE6245 1.0
import com.uit.STE6245.RigidBody 1.0 as RB
import "objects" as Obj

Entity {

    RB.Environment {
      id: rba_environment
      //gravity: Qt.vector3d(0,10,0)
    }

    RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,10,0)
    }



    Obj.MovingSphere {
        environment: rba_environment
        Component.onCompleted: initTranslation(Qt.vector3d(0,2,0))
    }

    Obj.MovingSphere {
        environment: rba_environment
        Component.onCompleted: initTranslation(Qt.vector3d(0, 4, 0))
    }

//    Obj.MovingSphere {
//        environment: rba_environment
//        Component.onCompleted: initTranslation(Qt.vector3d(2, 3, 0))
//    }

    Obj.FixedPlane { environment: rba_environment;
        Component.onCompleted: {setParametersQt(Qt.vector3d(-10,0,-10), Qt.vector3d(20,0,0), Qt.vector3d(0,0,20))
                                initPlane()}}

    Obj.FixedPlane { environment: rba_environment;
        Component.onCompleted: {setParametersQt(Qt.vector3d(10,0,10), Qt.vector3d(0,0,-20), Qt.vector3d(0,20,0))
                                initPlane()}}

    Obj.FixedPlane { environment: rba_environment;
        Component.onCompleted: {setParametersQt(Qt.vector3d(-10,0,-10), Qt.vector3d(0,0,20), Qt.vector3d(0,20,0))
                                initPlane()}}


}
