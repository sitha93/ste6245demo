import QtQuick 2.0
import Qt3D.Core 2.10
import Qt3D.Render 2.10

import com.uit.GMlib2Qt 1.0 as GM2Qt
import com.uit.STE6245.RigidBody 1.0 as RB
import "objects" as Obj

Entity {
    RB.Environment {
      id: rba_environment
      //gravity: Qt.vector3d(0,10,0)
    }

    RB.Environment {
      id: rba_environment2
      gravity: Qt.vector3d(0,10,0)
    }

        Obj.MovingSphere {
            environment: rba_environment
            Component.onCompleted: {
                initTranslation(Qt.vector3d(-3, 0, 0));
                setVelocityQt(Qt.vector3d(0, 0, 0));
            }
        }

    Obj.MovingSphere {
        environment: rba_environment
        Component.onCompleted: {
            initTranslation(Qt.vector3d(3, 0, 0));
            setVelocityQt(Qt.vector3d(0, 0, 0));
        }
    }
//    Obj.MovingSphere {
//        environment: rba_environment
//        Component.onCompleted: {
//            initTranslation(Qt.vector3d(0, 1, 3));
//            setVelocity(Qt.vector3d(15, 10, 0));
//        }
//    }
//    Obj.MovingSphere {
//        environment: rba_environment
//        Component.onCompleted: {
//            initTranslation(Qt.vector3d(0, 1, -3));
//            setVelocity(Qt.vector3d(15, 2, 0))
//        }
//    }
//    Obj.MovingSphere {
//        environment: rba_environment
//        Component.onCompleted: {
//            initTranslation(Qt.vector3d(0, 4, 0));
//            setVelocity(Qt.vector3d(5, 10, 0));
//        }
//    }

    Obj.FixedSphere {
        environment: rba_environment
        Component.onCompleted: {
            initFixedSphere()

        }
    }
}
